.. _scripts:

#####################
Notebooks and Scripts
#####################

All examples (both as scripts and notebooks) as well as grid files etc
are available in the `demo` folder of the git repository
https://gitlab.dune-project.org/dune-fem/dune-fempy
or can be downloaded here:

================================================= ===================================================== =================================================
Example                                           Notebooks                                             Scripts
================================================= ===================================================== =================================================
Introduction                                      :download:`notebook <concepts_nb.ipynb>`              :download:`script <concepts.py>`
Time Dependent Problem                            :download:`notebook <dune-fempy_nb.ipynb>`            :download:`script <dune-fempy.py>`
Adding boundary data                              :download:`notebook <boundary_nb.ipynb>`              :download:`script <boundary.py>`
Using different linear solver packages            :download:`notebook <solvers_nb.ipynb>`               :download:`script <solvers.py>`
One and three dimensional grids                   :download:`notebook <othergrids_nb.ipynb>`            :download:`script <othergrids.py>`
Parallelization                                   :download:`notebook <parallelization_nb.ipynb>`       :download:`script <parallelization.py>`
Checkpointing                                     :download:`notebook <backuprestore_nb.ipynb>`         :download:`script <backuprestore.py>`
Full Grid Interface                               :download:`notebook <dune-corepy_nb.ipynb>`           :download:`script <dune-corepy.py>`
Discontinuous Galerkin method                     :download:`notebook <discontinuousgalerkin_nb.ipynb>` :download:`script <discontinuousgalerkin.py>`
Bending beam (linear elasticity)                  :download:`notebook <elasticity_nb.ipynb>`            :download:`script <elasticity.py>`
Spiral wave (reaction diffusion system)           :download:`notebook <spiral_nb.ipynb>`                :download:`script <spiral.py>`
Slit domain (wave equation)                       :download:`notebook <wave_nb.ipynb>`                  :download:`script <wave.py>`
Saddle point solver (monolithic solver)           :download:`notebook <monolithicStokes_nb.ipynb>`      :download:`script <monolithicStokes.py>`
Saddle point solver (fieldsplit precond.)         :download:`notebook <fieldsplitStokes_nb.ipynb>`      :download:`script <fieldsplitStokes.py>`
Saddle point solver (uzawa solver)                :download:`notebook <uzawa-scipy_nb.ipynb>`           :download:`script <uzawa-scipy.py>`
Eigenvalue problems                               :download:`notebook <evalues_laplace_nb.ipynb>`       :download:`script <evalues_laplace.py>`
Adaptive FE (laplace problem)                     :download:`notebook <laplace-adaptive_nb.ipynb>`      :download:`script <laplace-adaptive.py>`
Adaptive FE (using DWR)                           :download:`notebook <laplace-dwr_nb.ipynb>`           :download:`script <laplace-dwr.py>`
Crystal growth (phase field model)                :download:`notebook <crystal_nb.ipynb>`               :download:`script <crystal.py>`
Time dependent surface (mean curvature flow)      :download:`notebook <mcf_nb.ipynb>`                   :download:`script <mcf.py>`
DG for advection-diffusion systems                :download:`notebook <chemical_nb.ipynb>`              :download:`script <chemical.py>`
DG for hyperbolic systems (Euler equations)       :download:`notebook <euler_nb.ipynb>`                 :download:`script <euler.py>`
HP adaptive DG (two phase flow)                   :download:`notebook <twophaseflow_nb.ipynb>`          :download:`script <twophaseflow.py>`
Virtual element method                            :download:`notebook <vemdemo_nb.ipynb>`               :download:`script <vemdemo.py>`
VEM method for Cahn-Hilliard                      :download:`notebook <chimpl_nb.ipynb>`                :download:`script <chimpl.py>`
================================================= ===================================================== =================================================



###############################
Mesh Files used in the Examples
###############################

================================================= =====================================================
Description                                       Grid (mesh) file
================================================= =====================================================
Unit cube grid file                               :download:`unit cube grid file <unitcube-2d.dgf>`
Sphere grid file                                  :download:`sphere grid file <sphere.dgf>`
Three quarters sphere grid with boundary          :download:`three quarters sphere grid with boundary <soap.dgf>`
Slit domain mesh                                  :download:`slit domain mesh <wave_tank.msh>`
Quadrilateral mesh                                :download:`quadrilateral mesh <quads.msh>`
Converter Gmsh-to-DGF                             :download:`convert gmsh file to dgf format <gmsh2dgf.py>`.
================================================= =====================================================

###################
Citing this project
###################

If you found this tutorial helpful for getting your own projects up and
running please cite this project:

Title: Python Bindings for the DUNE-FEM module
*Authors: Andreas Dedner, Martin Nolte, and Robert Klöfkorn*
Publisher: Zenodoo, 2020
DOI 10.5281/zenodo.3706994

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3706994.svg
   :target: https://doi.org/10.5281/zenodo.3706994

#################################
List of things that need doing...
#################################

.. todolist::


