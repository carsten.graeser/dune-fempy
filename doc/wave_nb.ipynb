{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7f7f70fd",
   "metadata": {},
   "source": [
    ".. index:: Equations; Wave Equation\n",
    "\n",
    "# Wave Equation in a Slit Domain\n",
    "\n",
    "In the following we will consider the wave equation\n",
    "\\begin{align*}\n",
    "\\partial_{tt}\\psi - \\Delta\\psi &= 0, && \\text{in } \\Omega\\times(0,T), \\\\\n",
    "\\psi &= g, && \\text{on } \\Gamma_D\\times(0,T), \\\\\n",
    "\\nabla\\psi\\cdot n &= 0 && \\text{on } \\partial\\Omega\\setminus\\Gamma_D,\n",
    "\\end{align*}\n",
    "where $n$ is the outward unit normal and $g$ is given Dirichlet data\n",
    "given on part of the domain boundary. We repeat a double slit experiment,\n",
    "i.e., we have a wave channel which ends in two slits on the right and\n",
    "forcing the wave on the left end by defining $g=\\frac{1}{10\\pi}\\cos(10\\pi t)$.\n",
    "\n",
    "We discretize the wave equation by an explicit symplectic time stepping\n",
    "scheme for two unknowns $\\psi^n$ and $p^n$ where $p=-\\partial_t\\psi$ based\n",
    "on\n",
    "\\begin{align*}\n",
    "\\partial_t \\psi = -p, \\\\\n",
    "\\partial_t p    = -\\triangle\\psi\n",
    "\\end{align*}\n",
    "The discretization first updates $\\psi$ by half a time step then updates\n",
    "$p$ and concludes by another update step for $\\psi$. So given\n",
    "$(\\psi^n,p^n)$ we compute\n",
    "\\begin{align*}\n",
    "\\psi^{n+\\frac{1}{2}} &= \\psi^n - \\frac{\\Delta t}{2}p^n, \\\\\n",
    "p^{n+1}              &=\n",
    "\\begin{cases}\n",
    "p^n - \\triangle\\psi^{n+\\frac{1}{2}} & \\text{in the interior}, \\\\\n",
    "g(t^{n+1}) & \\text{on }\\Gamma_D\n",
    "\\end{cases} \\\\\n",
    "\\psi^{n+1}           &= \\psi^{n+\\frac{1}{2}} - \\frac{\\Delta t}{2}p^{n+1}.\n",
    "\\end{align*}\n",
    "Note that the update for $\\psi$ does not involve any spatial derivatives\n",
    "so can be performed directly on the level of the degree of freedom\n",
    "vectors. We use *numpy* for these steps. For the update of $p$ we\n",
    "use a variational formulation\n",
    "\\begin{align*}\n",
    "\\vec{p} &= \\vec{p} + M^{-1}S\\vec{\\psi}\n",
    "\\end{align*}\n",
    "where $S$ is the stiffness matrix\n",
    "$S=(\\int_\\Omega\\nabla\\varphi_j\\nabla\\varphi_i)_{ij}$ and $M$ is the mass\n",
    "matrix, i.e., $M=\\int_\\Omega\\varphi_j\\varphi_i)_{ij}$. To simplify the\n",
    "computation we use a lumped version of this matrix, i.e,,\n",
    "$M\\approx{\\rm diag}(\\int_\\Omega\\varphi_i)$. In this form the algorithm\n",
    "only involves a matrix vector multiplication and no implicit solving step\n",
    "is required.\n",
    "\n",
    "The following test case is taken from\n",
    "https://firedrakeproject.org/demos/linear_wave_equation.py.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "f0ef82d4",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:20:05.080781Z",
     "iopub.status.busy": "2023-07-09T15:20:05.078290Z",
     "iopub.status.idle": "2023-07-09T15:20:06.024888Z",
     "shell.execute_reply": "2023-07-09T15:20:06.023188Z"
    },
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "import numpy\n",
    "import dune.fem as fem\n",
    "from dune.grid import reader\n",
    "from dune.alugrid import aluConformGrid as leafGridView\n",
    "from dune.fem.space import lagrange as solutionSpace\n",
    "from ufl import TrialFunction, TestFunction, grad, dot, dx\n",
    "from dune.ufl import Constant, BoxDirichletBC\n",
    "T = 3\n",
    "dt = 0.005\n",
    "t = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03f43b27",
   "metadata": {},
   "source": [
    ".. index:: Grid construction; gmsh (from file)\n",
    "\n",
    "We use *gmsh* to define the domain and then set up a first order\n",
    "scalar Lagrange space which we will use both for $\\psi$ and $p$. We can\n",
    "construct general grids by either defining the grids directly in Python\n",
    "(as demonstrated in the following example) or by reading the grid from\n",
    "files using readers provided by Dune, e.g., Dune Grid Format (dgf) files\n",
    "or Gmsh files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "8a4702f5",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:20:06.031827Z",
     "iopub.status.busy": "2023-07-09T15:20:06.031163Z",
     "iopub.status.idle": "2023-07-09T15:20:07.780289Z",
     "shell.execute_reply": "2023-07-09T15:20:07.778968Z"
    },
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "domain = (reader.gmsh, \"wave_tank.msh\")\n",
    "gridView  = leafGridView( domain, dimgrid=2 )\n",
    "gridView.hierarchicalGrid.loadBalance()\n",
    "V = solutionSpace(gridView, order=1, storage=\"numpy\")\n",
    "\n",
    "p      = V.interpolate(0,name=\"p\")\n",
    "phi    = V.interpolate(0,name=\"phi\")\n",
    "pVec   = p.as_numpy\n",
    "phiVec = phi.as_numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cca516ed",
   "metadata": {},
   "source": [
    "Next we define an operator for the stiffness matrix including the\n",
    "boundary condition which are time dependent so we use a `Constant` for\n",
    "this. We use the `BoxDirichletBC` class which is derived from the more\n",
    "general `DirichletBC` class which takes a function space, the boundary\n",
    "function $g$ as a ufl expression, and finally a description of the part\n",
    "$\\Gamma_D$ of the boundary where this boundary condition is defined.\n",
    "This can be a ufl expression which evaluates to $0$ for\n",
    "$x\\not\\in\\Gamma_D$, e.g., an ufl `conditional`.\n",
    "\n",
    "Note that the stiffness matrix does not depend on time so we can\n",
    "assemble the matrix once and extract the corresponding sscipy*\n",
    "sparse matrix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "154d4d9f",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:20:07.785843Z",
     "iopub.status.busy": "2023-07-09T15:20:07.785296Z",
     "iopub.status.idle": "2023-07-09T15:20:08.420200Z",
     "shell.execute_reply": "2023-07-09T15:20:08.418914Z"
    },
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "u    = TrialFunction(V)\n",
    "v    = TestFunction(V)\n",
    "p_in = Constant(0.0, name=\"g\")\n",
    "# the following is equivalent to\n",
    "# x    = SpatialCoordinate(V)\n",
    "# bc   = DirichletBC(V, p_in, conditional(x[0]<1e-10,1,0))\n",
    "bc   = BoxDirichletBC(V, p_in, [None,None],[0,None], eps=1e-10)\n",
    "\n",
    "from dune.fem.operator import galerkin,linear\n",
    "op        = galerkin([dot(grad(u),grad(v))*dx,bc])\n",
    "S         = linear(op).as_numpy\n",
    "lapPhi    = V.interpolate(1,name=\"e\")\n",
    "lapPhiVec = lapPhi.as_numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21aa2935",
   "metadata": {},
   "source": [
    "Next we multiply the inverse lumped mass matrix to $S$, i.e., we form the\n",
    "matrix $\\Delta t M^{-1}S$ where $M$ is a diagonal matrix with entries\n",
    "$m_{ii} = \\int_\\Omega\\varphi_i$. To compute these values we construct the\n",
    "mass operator $<L[u],\\varphi_i> = \\int_\\Omega u\\varphi_i$ and apply it to\n",
    "$u\\equiv 1$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "9ff81c00",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:20:08.424743Z",
     "iopub.status.busy": "2023-07-09T15:20:08.424529Z",
     "iopub.status.idle": "2023-07-09T15:20:08.782205Z",
     "shell.execute_reply": "2023-07-09T15:20:08.780974Z"
    },
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "from scipy.sparse import dia_matrix\n",
    "lumping = galerkin(u*v*dx)\n",
    "lumped = lapPhi.copy()\n",
    "lumping(lapPhi,lumped)       # note that lapPhi=1\n",
    "N = len(lumped.as_numpy)\n",
    "M = dia_matrix(([dt/lumped.as_numpy],[0]),shape=(N,N) )\n",
    "S = M*S"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e441539",
   "metadata": {},
   "source": [
    "We can now set up the time loop and perform our explicit time stepping\n",
    "algorithm. Note that the computation is carried out completely using\n",
    "*numpy* and *scipy* algorithms we the exception of setting the\n",
    "boundary conditions. This is done using the `setConstraints` method on\n",
    "the stiffness operator which we constructed passing in the boundary\n",
    "conditions. At the time of writing it is not yet possible to extract a\n",
    "sparse matrix and vector encoding the boundary constraint."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "b0708d81",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:20:08.788041Z",
     "iopub.status.busy": "2023-07-09T15:20:08.787497Z",
     "iopub.status.idle": "2023-07-09T15:20:26.165220Z",
     "shell.execute_reply": "2023-07-09T15:20:26.163994Z"
    }
   },
   "outputs": [
    {
     "data": {
      "image/jpeg": "/9j/4AAQSkZJRgABAQEAMgAyAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADrAP8DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+qGoatFp1zZwyxSN9qlESMrJwxIA4LBj1z8oOACTgCr9VbrTra9lhkuBIxiZXVRK4TcpDAlQcNggEZB6UAMudY0uyn8i61Kzgmxu8uWdVbHrgnNRf8JDon/QY0//AMCU/wAaJ/8AkZbH/rzuP/Q4a0qAM3/hIdE/6DGn/wDgSn+NH/CQ6J/0GNP/APAlP8a0qKAM3/hIdE/6DGn/APgSn+NB8RaGoydZ08D1N0n+NaVUtX/5Bc3/AAH/ANCFRUlyQcuxFSfJBy7Ir/8ACTaB/wBBzTf/AALj/wAaP+Em0D/oOab/AOBcf+Nc9RXm/wBpP+X8Tyf7Vf8AL+J0P/CTaB/0HNN/8C4/8aP+Em0D/oOab/4Fx/41z1FH9pP+X8Q/tV/y/idD/wAJNoH/AEHNN/8AAuP/ABo/4SbQP+g5pv8A4Fx/41z1FH9pP+X8Q/tV/wAv4nQ/8JNoH/Qc03/wLj/xo/4SbQP+g5pv/gXH/jXPUUf2k/5fxD+1X/L+J0P/AAk2gf8AQc03/wAC4/8AGj/hJtA/6Dmm/wDgXH/jXPUUf2k/5fxD+1X/AC/idD/wk2gf9BzTf/AuP/Gj/hJtA/6Dmm/+Bcf+Nc9RR/aT/l/EP7Vf8v4nQ/8ACTaB/wBBzTf/AALj/wAaP+Em0D/oOab/AOBcf+Nc9RR/aT/l/EP7Vf8AL+J0P/CTaB/0HNN/8C4/8afF4i0SaVIotZ0+SR2CoiXSEsTwABnk1zdRy/620/6/Lf8A9HJV08wc5qPLuXTzNzmo8u77ndVBLe2kF3b2s11DHc3O7yIXkAeXaMttB5bA5OOlT18teK/iDf6t8QV8Qadcv9n06fGmoWYJ5a8EkDacSYJYHna20ngV3zmoLU9/DYaWIk1Hoj6lorN8P65Z+JdCtNY08uba5QsokXaykEhlI9QwI9OOCRzVgalYMlw63tsUtiROwlXERHUNz8v41ZztNOzLVFU01bTpJEjTULVpJI/NRRMpLJ13AZ5HB5oTVtNkWBk1C0YXBKwlZlPmkHBC88kH0oEXKKgtry1vA5tbmGcRsUcxOG2sOoOOh9qnoAKKKKAM2f8A5GWx/wCvO4/9DhrSrNn/AORlsf8ArzuP/Q4a0qACiiigAqnqqM+mThRkgA/gCCf0q5VPVnaPSrgqVBKbfmzjnjt35qZxUouL6mGJaVCbfZ/kcnuFLuFVPnpd7jtXnPLk/hkj4RY5reLLWaWqomI61KsoNc9TBVIa2N6eMpz6ktFNDZp1cbi1udSknsFFFFIYUUUUAFFFFABRRRQAVHL/AK20/wCvy3/9HJUlRy/620/6/Lf/ANHJWtD+LH1Rth/40PVfmdL4miSfwprEMgyj2MysM4yChBr5A8mvsLxD/wAi1qv/AF5zf+gGvkvyfavWxcuWx+n8O0fa+08rfqe9fA+yhg8DvOgcSS3cm/8AeMVJAUZ25wDgDkCuoi8LzRLMgv4yhWJYs25yvlSmWPcd/wA3LMG6buMbec4XwZG3wGB/09y/0r0Guqn8C9Dw8arYmou0n+ZzMvha4uIzb3GoxvaNFODGtuQwlm8zfIp34H+sIAIOBnk5yHHwvM939qlv4zLLMstxttyFba8bKEG47P8AVgE/NnJPFdJRVnMZOiaK2kIVe5E2IYreMiPZiOMELnk5bk5PGeOBitaiigAooqCW+tILmK2muoI55f8AVxPIAz/QHk0AVJ/+Rlsf+vO4/wDQ4a0qzZ/+Rlsf+vO4/wDQ4a0qACiiigAqlq7SppF0Ydu/YR8wJGO/T2z/AF4q7VDW3KaHfbW2s0Dqhx0YjA/UiqgrySOfF/7vPW2j17aHC+bcL1CH6Gl+1Oo+aJvw5rD+z60h3faEYelKb3Vbc/vrYOvqte08tpz25X87H5Wp1F8M/wAf8zeS5il4zg+h4p+wHlTWEmsWsxCTxtG3+0KtpI6ANby+Yv8AdJrlqZZKHwtx8nqvvL9u1pUiaYdkPNTJIDVGC+jlOxxtf0NTlccqa8jE4TXlqx5X+B2UcRKOsHdFsHNLVeOXsanBzXiV8PKlKzPWo141FdC0UUVzm4UUUUAFFFFABUcv+ttP+vy3/wDRyVJUcv8ArbT/AK/Lf/0cla0P4sfVG2H/AI0PVfmdR4h/5FrVf+vOb/0A18vfYzX1D4h/5FrVf+vOb/0A188fZvau3NKnJyfP9D9c4Skl7a/93/249Y+EDqvhF7bD71uXYny224OP4sYJ9s5rfh8TvLBcSfYlDKIDAvncOJpWiTcdvyHK5P3sA9+lZXwsXZ4TlX0u3/ktdWNI00NMw0+03TBhKfJXMgYgtu45yQM564r0cO70ovyX5HzOY/75V/xS/Nmdb69dXE8cKaehd7e5cAXHJlhkWNkGVA2ktw2fqBUEXiW/P2Np9GEMbz/ZrxvtQb7LIWVUXAX5928cjgZ5Na50fSzJFIdNsy8URhjYwLlIzwVBxwp9OlEej6ZEYDHp1ohgJMJWBR5ZJydvHHPpWpxlPw9rp120adrRrUlUljRn3M0TruRjxxkZ47YrZqC2s7WzV1tbaGBXbewiQKGb1OOpqegArNv9Pub3ULOQXUS2cDeY9u0JYyODlTuDDGOoGCM4PYY0qKAMmWNh4stJDM7K1lOBGQu1cPDyOM8+5Na1Zs//ACMtj/153H/ocNaVABRRRQAVl+I4bqfw7fx2dx9nnMLFX2BunJGD6gEZ7Zz2rUrO14uPD2peW6pKbaRY2Y4G4qQv64rSi2qkWu6McRb2M79n+R4+bXxDEARcqwpf7U1m1H7+1Ei+q1VP/CSw9XRx6ULruo2xxeWRK9yozX2nsnNaqEvTQ/MOSUtlGXpp/kXV1exuwUuIvLkP94YpfKMP7y1mOPTPFVxd6bqYOQFf0PBqExPbNmNyU9CaI00tFdeTM/Zq9ldeTNWOZZ12yDbIKu2upNbuIbo5U/df/GsHzJBiQLn6Vo20kV/CYzjd6VzYnDQnC01eP5GbTpvmWx0XDAMpqWKTsa56zvZNOuBaXJPlt9xz/Ktw9mWvlsbgXD93LVPZnXSrOL54lwHNLUMT5FTV8tWpOnKzPdpVFON0FFFFZGoUUUUAFRy/620/6/Lf/wBHJUlRy/620/6/Lf8A9HJWtD+LH1Rth/40PVfmdR4h/wCRa1X/AK85v/QDXiH2b2r2/wAQ/wDItar/ANec3/oBryf7N7U8/ny+z+f6H6fw7U5PafL9Tufhsuzw5Ovpdv8A+grXY1yHw+Ey6ZeIY0EIuWw+87idq5G3GMe+fwpYbTXlivYnjvSZBES/2pTvZZSZfLO/Kb4yAv3QNp+7wT7WDd8PTfkvyPDxjviaj/vP8zrqK5mxttUh1OyM9vfvAbW5jlJuwVTMoaFWHmcuEBUuATk/ePWmWcOrJDoJks9SDwXUouQ92rYiKuFMn7w7+TGf4iNp/HoOY6misnQ0u4hfpdQXUafamaA3EwkJQgYwdzEDOeDjHpWtQAUUUUAZs/8AyMtj/wBedx/6HDWlWbP/AMjLY/8AXncf+hw1pUAFFFFABWR4pa8XwvqJsGgW5MJCmckLg8Hp/FjOPfFa9ZXiVrtPDGqNYoj3P2aTYHcqPunJyAeQMkDuQBkZyNcP/Fj6rfbcxxGtKVuz/I8T83xLBy6RyD0FKviCZP3d/Zsg9cZFVP7T8S2wzPaLIvtUsXiKC6Hk3luYnP8AeFfeuk3q4J/4T83dFtXcItf3WTyx2N4olgIVzzlaZDJJ/qnbPpVaWOGKTzIGwp6gVMYJJYvNgOWAz9aLJK19PMOVJWb08+hbhuzZyBJl/dt0ard1ZTwqNQsecDLKO4qDTJbfV4HtJgFnXgg1Jpl7LpGonTL4kxP/AKpz0+lctVyTfIveW67o55RfM7L3luu6NWF7bX9M6gSAc+qmnaPeSK72Fyf30XQn+IVl3sbeH9WW/hB+yTHEqDoD61f1ePKw6panLJgnHda82pShNez+xP4X2l2JSUbcuz2/y/r1N1G2vjtVtTkVl29wtzbpKp6itCFsrXyOaYVxXM1qtzuy+v7zgTUUUV8+e0FFFFABUcv+ttP+vy3/APRyVJUcv+ttP+vy3/8ARyVrQ/ix9UbYf+ND1X5nUeIf+Ra1X/rzm/8AQDXn32b2/SvQfEP/ACLWq/8AXnN/6Aa5X7N7VycVT5fZf9vf+2n6DlVTk5/l+preCV2aZer6Xbf+gJXTVz/hJdlrfr6Xh/8ARaVrDUrBkuHW9tilsSJ2Eq4iI6hufl/Gvo8A74Sk/wC7H8kebiHerJ+b/MtUVVTU7CSSKNL62Z5k8yJVlUl15+ZRnkcHkelImq6dIkLpf2rLM+yIiZSJG9F55PsK6zIt0VWtdQsr5pFs7y3uDEcSCGVX2H0ODxVmgAoorJ1+5uLS1tpbZrrf9rhVkt7czbozIocMArEDZuORjp1oAkn/AORlsf8ArzuP/Q4a0q5DxZ4ifw3qSXz2scyxabeSxIJyrSFPKdgflOOF68/SuQ0r45DVE04r4e2NeXxtCv23d5Y/d4b/AFfOd544+719BaxlNbRaT8r7f8PsNJuSj1av8le/5M9eorxG3/aFa4gaUeFThXCHF/nkjj/ll7H8qtf8L6TyL9z4eG+0ukgCfbv9YGEmW/1fGNg4/wBr2rWrRnR5+dW5LX8ubb7/APhzSNGpPk5V8d7edt/uPZKyPFJuF8K6o1pBFPcLbOyJK20ZAznoeR1AxyQBkdR5xc/HIQHVseHt40++W0B+248zPm/N/q+P9V05+9145p6p8b2vLC5tLLw+0kksSw+YLrIR5YgVGNnJB3jHGdnvw6Caqp225W/m1b77rzMqlCc6bile/MvuV3f5HLDxHrlqf9O00lO5UVM+o2OrQ58va/cEciqCeM7lSIdU054S3G4jimXU8ETreImIyfmIHFfovsLPWHK+lnoz89+rNS1p8r6OL0ZesVjklNuzc9gasJcTeH9QjFwC1nKcBv7tF1o7X2lLqWmuPOjG4Y7+1aGkXFt4r8PyW06gXEYKsp6g1zVasVFzesb2l3Xmc1Saced6x2kuq8xNcsHtzFremclQC4Xoy1eu0h8SaEl1AcTINykdQazPC99JEtzol9y0WQhPdaXR3bR9YuLMnNvISyj0rlnTqLS/vQ1T7x7GE4yjeN/ehqn3Rq6ZdLrOjPa3IHmoNjA+tLoFxiKbTLg5MeQue61mxt9i1qSSPAjl5Iqdjtv/AD4uves6mHi1KK2lqvJmMo7pbPVeTNHSpDbyTWzH5VbK/St61k3Cubhk8y8LjqRzWvp0uZGT0Oa8nNsN7SlKXWxFKbhWUjaHSlpq9KdX53JWdj6mLugoooqSgqOX/W2n/X5b/wDo5KkqOX/W2n/X5b/+jkrWh/Fj6o2w/wDGh6r8zqPEP/Itar/15zf+gGsryRWr4h/5FrVf+vOb/wBANZm6uDi6Dn7G397/ANtPr6Vb2V/Mm8NQIkupygvuN1tIMjbceWn8OcA++M1FF4XmiWZBfxlCsSxZtzlfKlMse47/AJuWYN03cY285s+HDldS/wCvz/2nHW3X02XK2DpL+7H8kZylzNs5aTwb54kSW/PlzhzOEi2sXbzslDu+Ufvm456DnrSL4NxcW0/2yFXhkLbY7YqiAtGcRDedhPlcnnO9uOa6qiuwRTsbH7I93K0nmS3M5ldtuMDAVVA9lVR7kE8Zq5RRQAUUUUAeUfGRXa80MDPl/Zr/AM30CbE3Z/DNeUfDKKKQytIFzHqdi6568ednH6V7V8Vk3aTM4CmRNJvipPbIiDY/4CTXh/g+1uJ9Ft7u2H7vTr6a5vCDjbGIkKk/98SAfjTcPa4DEU27Jygv/SX+N7LzOmliv39OCXwxl+U3+lil4QltIdPle7QNGuq2JbP9zE26qlpZ/wDEs1Z7g7biHUIImY9BuE27P4oPyq74O0/7XbQxTD9xdatbRkj0UPu/SQVRsbvztC1IXDHzru/glDEcNtWbefwMi/nXVjm1LH26uj+n9M68I23hOT+/+dv0dzV1Rfm8YEgKRr0YwO3/AB9VV+2Lp0oaxYyYntpZYCvIliiXH5s8ox7U+/vkuW8UEgrJcazHMFPYf6Rn/wBCFIt0LS5S/sWNzdtfR3M0IU7d+xHQZHPLtMuAc/L7VDjOFTERcdV7PTpdcnk/kzDDxi6FByu+bn+a5Xq/uOgTxCPFWnTJ9i2zoOQBnFXfB0llr1hcaZcgLPGCpU9azfBuoTaRqeo6jrdo1vHdkvlo9oDEknAP1pdLs5J9cvdd0k4twx4HQ+tfoUo3pypxXKlZp+fY/Nq9GnBVKUPdirOLvdX7XNHwtez+HNbutDuiXtySYifSpFzpHiiW4tuIJ+XA6Zqvo3/FTX9zc4xJFxxU/h/Oo6xeWknzNCcc1NVRjKc5b8q5v8znrK05zlvyrmX6k1w3m6sLuD7+OcVLEWvbwt0dKZoybvE93Yt/B0qXTh5fjO4sz90rmspyUbrtG/yOeel0ukbr0J7X/SLx4m5dasWAJ1KS2cc44qO3UQeNHg7MucVOMweNFTs6Vy1J3ul/Lc5pq90usbk1kBHrkluwxxkCtKE+Tq5TswrOuv3XjK1A6SRmr918mtwH+8K8+tL2jX96FzCcdpeX9fkdFH90U+o4vuipK/M66tUaPp6LvBBRRRWRqFRy/wCttP8Ar8t//RyVJUcv+ttP+vy3/wDRyVrQ/ix9UbYf+ND1X5nUeIf+Ra1X/rzm/wDQDWNurZ8Q/wDItar/ANec3/oBrn99XntH2ns/K/6HuY2r7Pl+ZpeGLiFn1OASoZlutxjDDcAY0wcdcU2HxO8sFxJ9iUMogMC+dw4mlaJNx2/Icrk/ewD36VJ4XOYNQP8A0+H/ANFx1pDSNNDTMNPtN0wYSnyVzIGILbuOckDOeuK9jCLlw8F5L8jrpO8E/Ix4PFEtxdpbrYRht5hkLXBwsoMowPl5XMJ+bjg5xwalg1+6mjsn+wwEXM5iAjuSxZQ2PMj+Qbkx82Tt46ZyM6baRprBA2nWhCRGFcwr8sZBBUccLgkY6cmmR6HpETwPHpdkjQZ8llt0BjycnbxxyAePSugsTSdQm1CO4aa3jiMMxiDQymRJMAZKsVXOCSp46qa0KrWWn2WmweRYWdvaxZz5cESovp0AqzQAUUUUAed/FdhHo88mCSNJvlAA/veUv9a8X8D30Vh4U1GzdSX1x5bJGB+4ViGD+cor1v4v3r27abaKMrfWV/btn3SPB/A4P4V4/wDDy3jv40S4b5bXU4PJHT5pgwP/AKKT8qqMo08DiJz0XPBv09y33NX9DSOFlOtBz1jKMrL0U/1tYh8E3qrFpkJYKsWsoXJ6ASBQD/5DasSxEs/hiTlRFZ3o7clpkP6DyP1q54WspL3TZ4Y22mTU7KMH0LCYZotpY7jRddkQCKOXVLZlX+6CtzgfgDXTjn72Ot0dL8bfmduFiovCNK/x/m//AJJi6hayNe+I7pwFaHVREyj1czH/ANkNSWjXOiRRS2zR3PnX0M4hCkt5kUauo49ftBXHqtW9Wbc/jJsYJ1+M4/8AAqqQludKvxdQGCa4luobxYQpJDBFkQfQ+aRj/ZqZznWqYhtK/wC70/8AANL69WZ4bleFw9NvRKatbdcrfy+86NdX1D4kwS6d9ljtjD8zEE/1qXw7fJ4btL3Q5T5lxk9O/FVPDc+saRJqGq3FtskumLsCMckk9Pxq54I0pr7VrvXdUI+YkqGr76VOFKlKMkvZpKyW99z83xKpwp1IaKkrNJO95epJ4GY6PPf/AGgbWkOQDU/hA+T4n1O6cYSQnFVLZm17xhcJZqfskPDMOhNW5j9n1z7FbDLkfNjtTrRU3NP4pxV/JHPXvKU09JTim/IsaIQ3jm/uf4CMVNYnzfiDcTD7oXFVkzp16R/y0k61LYZtdRluJOGesKkFLmkuseVHNU15pLrFJF5CJPHxcdFTFTTnzPHMJA+6mDVWxbGsy3bfxDip7NhJ4he6PQDArlnCzbXSFjnk7P0jYtX/AM3jGxx/ChzWhe861a+wrOjb7R4n87si4FaRHm60h7KtcFSPI4X+zBmE5aKPl/n/AJm/F90VJTI/uin1+a13eoz6airQQUUUViahUcv+ttP+vy3/APRyVJUcv+ttP+vy3/8ARyVrQ/ix9UbYf+ND1X5nUeIf+Ra1X/rzm/8AQDXM766bxD/yLWq/9ec3/oBrkt1exiqPtLeR1Z7V9n7P5/odB4UObbUD/wBPh/8ARaVv1zXhBpjHqIKRiH7UcOHO4nYmRtxjHvn8Km1O61xNoNultaGXD3FlI1xOqc4IjMWOTgH72Mn6joprlgkexhHfDwfkvyN+iuWvNQ1v7Dp5jjuY7r7PmdY7bcGudsZWNiVOIzufLDAGMbgRiqk+pa2ltfb31NZdyrD5NiGCSl5QR/q2zEFCEtgn0JJxVnQdpRXKXOp3sdxqRWXWTDBGoVYtPyzyEr/qsxEFecEtu6k8BcnotNM7aXaG6fzLgwp5r7Cm5sDJ2kAjnsQMelAFmsnUdaaw1CG2W2EiMYvNcybSoklEa7Rg7uTk8jA9c1rVDNZ2txNFNNbQySwkmJ3QFkJ67SenQdKAPO/ihpsmsXFvHbW1zPc2mm38saRQu25ykYUKcYY57CvIfCnh/wAR6ObF5NB1lAdTinukOmTHZHFjaw+XnO+Xgc/L7ivpaf8A5GWx/wCvO4/9DhrSo3o1KMtVNpv5W/yubRrTjKMr/Cml87/5/gfI2k+FvGFnp04j8OawhF1DKM2UgO5RJtIGMnqee3HqKePCHiBo9UKeHtaUveI8EZ06Ybo/3mTnbxjKcHn5vY19bUV0V6/tvbXVvact7f3bW/LUulipU3Sa+xzW8+be58sX/hrxI83iSNtA1d5LnVlnjK2EpWRR5+WDBcY+de/OeKrx6B4i0jzLn/hHdTljSW2uWmksZU2eVGSynK/dy5GenyelfV9Y3i2//svwlql39mnudluw8qBNzHd8ucegzknsAT2oo+/Pktfm5F/4C1b8jKVdwpuyvbmdv8StY+c7nU/EGvgRrYfZ4T1Jqef7bb2IsIn2lxtJFWn8TaleL5dnpTrnjLDApIdPvSxubxgH649K/Rb8qSlFRS2W5+d83IkpRjFLZXvqammvY+EPDzsMNO4yT3ZqTwnp8gW513UhtebLAH+EVmw2y3t4r3Lbo4zwD0q9qmoT6oU0jT/ljPEjjoBXJUpt80U9Z/FLsuxyTjJ80L6y1lLsuwmkRvruu3F/gi2iJVCehqUL/aGutDF/q4vvEVoXlxB4Z0BbO2AM7jaoHUmn6NZpomhSXt2R58gLuT61zzxHuuolo/diu/mYyqXTqR2+GK/X+upTlGL8W8fJxzipgPs8wVfvntUnh22Myz6pcjHmZK59KfpMX9oahPd/8sUJC+9TOrGPMntFa+vYym7NrstfUfaqYrok/ePNa+nx7rl5D9Kz7RftN9M6j5F+UVuWUWwdOvNeTmeIVOlLvYwhFzrJehoJ0p1IOlLX5vN3k2fVwVo2CiiipKCo5f8AW2n/AF+W/wD6OSpKjl/1tp/1+W//AKOStaH8WPqjbD/xoeq/M6jxD/yLWq/9ec3/AKAa4zfXZ+If+Ra1X/rzm/8AQDXnn2uvrsLQdW9uhjxZNx9jb+9/7adl4OObG+/6+z/6LSujrmfBD79MvG9btv8A0BK6C5ureygae6nighXGZJXCqM8Dk1z1I8s3HsfSZe74Sk/7sfyRNRVea+tLcQme6giEzBYt8gXzCegXPU/So21bTVSZ21C1CwNsmJmXEbZxhueDnsag7C5RVSTVNPhaRZL61RolDyBplBRTjBPPA5HPvU1vcwXdulxbTRzQuMpJGwZWHqCODQBLRRRQBmz/APIy2P8A153H/ocNaVZs/wDyMtj/ANedx/6HDWlQAUUUUAFZPii5Sz8LancSCUqlu5IijLk8dMAHg9CewycjGRrVleJbiO18M6lNL5mxLdywjQuSMdMAHj1PYZORjNa0FerHS+q/MxxH8Gfo/wAjwpvFhddtppkpJ6fLiozHrGpndOggi9B1q6fE1rgC3sZCfZMU1r7V9Q+WK18lD3avvknDWMFHzbPzlJw1jTUfNu5WktVt0CBuat2k0GmQF1XLn8yaQab5C+ZcS7n9zUcaxvLk8qKG1NWbuhSkpxs3dFrS7J76+/tLUOi/cQ9hU148niPU0s4eLOI5kI6H2qJ3nvCLaDKofvN7VpCW20GwEcQDTPwAOpNclVyUlJay2iui8znlNqV/tbJdg126EMEOkWQ/ey4XC/wrVqdY9D0VYIh+8YbR6kmotF08w+ZqV8czvzk/wiltVbWdU+1Ov+jQnEYPc+tedJwj7t/chrJ/zS7EaW5Vqlv5v+tPvZf0y0+yWSKfvtyx961oFwKhC7mAFW0XAr5XNcW5x13ep14Gjeo5sfRRRXzR7oUUUUAFRy/620/6/Lf/ANHJUlRy/wCttP8Ar8t//RyVrQ/ix9UbYf8AjQ9V+Z1HiH/kWtV/685v/QDXkf2n3r1zxD/yLWq/9ec3/oBrwr7V71+hZHT5/afL9Q4mpc/sv+3v0PV/h7GraVd3GX3tcspBkYrgKv8ADnAPvjNa914eR547q0uriO6jl81DcTyzxZIIP7tnx0Y4xjH6VjfDNt/hmdvW7f8A9BWuyrx8UrV5rzf5n0GBVsLTX91fkjnZvDDyWVpaLeqsVvamyBMOWaFhGG53cPmPIboM/dOM1Xm8G+ccvdwybHBhSW23IFHncON43n9+3ORyoOOueqorA6jm7PwtLp91dXFtqJEs0DxrK8O6TeyoA7tu+fBjyBgfePNb1naxWNlBaQKVhgjWNATnCgYFTUUAFFFV5b63gvLe0kZxNcbhF+7YqSASRuxgHAJwT2oAqz/8jLY/9edx/wChw1pVmz/8jLY/9edx/wChw1pUAFFFFABWb4hbZ4c1Fy21Ut3d/lzlQCWGPcAj8a0qzteuLe28P6hLdOiwi3cMXOAcjAH1JOPxrSj/ABI27oxxKTozT7P8jxX/AISLTl/1NoxPslJ/a9/eEpa2LIP7zCra61o8TZjtwfotNOvSStizsXPoduK+1UXuqT+bPzDkS1VP72Vo9Ju5z5l9Nx/dHSnSCGL93HgkelTC11W9O6Y+Uh7CpRaW9mnLbn+vNP2t3Zu77ITqa+87+S2II5jDHiJPmNW9PsVaf7Vdnc/bPaiCBm/eMu1asJDLeHyoeE6M9Y1qiUXrbuzGU7u0eu4s7y6rP9kt/lhB+dh6VtwQR2kCxRrgAYpLa1isoAkY+p9anRCxya+XxmNjUXLDSnH8WdFOk1aK3Hwp3NWBTVGBTq+TxNZ1Zts93D0VThZBRRRXObhRRRQAVHL/AK20/wCvy3/9HJUlRy/620/6/Lf/ANHJWtD+LH1Rth/40PVfmdR4h/5FrVf+vOb/ANANfOP2n3r6O8Q/8i1qv/XnN/6Aa+WPtPvX6dwxT5/a/wDbv6noZxT5+T5/oe+/Ci5hk8MSwiWMzC5djGGG4DCgHHXFdFdeIUSeO1tLW4kupJfKQXEE0EWQCT+8ZMdFOMZz+tc38H23+CC3rdyfyWu5ubW3vIGguoIp4WxmOVAynHI4NfPY5WxVVf3n+bPUwytRgvJfkYtz4m8qwsbyG03pc2bXzh5NpSJdm7GAdzfvBgcA4PIqSbXLmG31GQ2URayvFtyv2g4dGWNg+dnBxIPl9utaL6ZYSFTJZWzFXEilolOHAADDjqAAM+gHpUT6HpMi3Cvpdky3LiScNboRK46M3HzH3NcpsV7nVrmC41WFbSJ2s7WO5jJnIEobeCD8h2kGM9N2cjpWlbTfaLWGbbt8xA+M5xkZqu+j6XJLNI+m2bSTxiKVjApMiDorHHI4HB4qzBBDbQJBbxJFCg2pHGoVVHoAOlABNBFcwvDPEksTja6OoZWHoQetZo8O2MNzZy2SiwjtpWm8i0ijRJGKlSWG3PQkcEVrUUAZMsKr4stJgX3PZTggyMV4eHoucD8BWtWbP/yMtj/153H/AKHDWlQAUUUUAFUdaKjQ78vjYLdy+RnK7Tn9M1eqlrDqmjXhaN5FMTKVUZOCMflzk+1XT+NephimlQm32f5Hkn27Rk5S3DH2SnjVwcLaWDE/7uK1FhtlIK2//jlSru/5ZwY+vFfQSxFLdxb9ZH5PzReyuY4j1W8++RCnoKlTT4bUgufMl9+TWqLeaQ/PJtHov+NSx28UPOMn1PWuapmkI+7H7o/5mipVJLayKEdg85Bk+SP+6OprRRI4ECooAHYUpYnhRT0i5ya8nFYydRXquy7HTRo9Ker7jVQucmrKrgUKuKdXgYnFOq7LY9fD4ZU1d7hRRRXEdYUUUUAFFFFABUcv+ttP+vy3/wDRyVJUcv8ArbT/AK/Lf/0cla0P4sfVG2H/AI0PVfmdR4h/5FrVf+vOb/0A18f/AGn3r7A8Q/8AItar/wBec3/oBr4u3V+s8HJP21/7v/tx72LgpWufT/wUbf8AD5W9buX+leiV5n8DWmPgBQUTyftMuHDncTkZG3GMe+fwrrdTutcTaDbpbWhlw9xZSNcTqnOCIzFjk4B+9jJ+o+WzL/fa3+KX5s6aatBLyN+iuWvNQ1v7Dp5jjuY7r7PmdY7bcGudsZWNiVOIzufLDAGMbgRior691ZrbV0tr7UIJbe+/0dzpbSGSPygdi4TBXfuG/nGBknIzxFnXUVx15eeIUuL0W09yyyCMgGwOLLLxq4U7f352mRuM42e4rpdJluJtJtJLtXW4aJTJvXaScdSMDGeuMcZxQBcooooAzZ/+Rlsf+vO4/wDQ4a0qzZ/+Rlsf+vO4/wDQ4a0qACiiigAqnqzhNKuSX2fJjOM5zxj8en41cqrqTiPTbgsCQUK8e/H9aUmkrsxxKvRmvJ/kcPvX0o3nsKn8oUojHpXE8XQXS5+frC1n1sV/nb2pywk9asBRS4rCeYO1oKxrDArebuMWMCngYpaK4J1ZTd2zthTjBWQUUUVmaBRRRQAUUUUAFFFFABUcv+ttP+vy3/8ARyVJUcv+ttP+vy3/APRyVrQ/ix9UbYf+ND1X5nUeIf8AkWtV/wCvOb/0A18U7q+1vEP/ACLWq/8AXnN/6Aa+JN1fpfDVb2ftfPl/U+jrK9j6j+Bhz8N4v+vqX+Yr0qvNPgTz8NYv+vqb+Yr0W5ureygae6nighXGZJXCqM8Dk14ONfNiqj/vP8zWPwomoqvNfWluITPdQRCZgsW+QL5hPQLnqfpUJ1nSlillOp2YjifZI5nXCN6E54PtXKUXqKqPqenxvOj31srwKGmVplBjB6FueByOvrViKWOeJJYZFkjdQyOhyGB6EEdRQA+io5hK0LiB0SUj5GdCyg+pAIz+YrBlt9ZOr2An8yZVcvLdWhMMSr2RommO4k5JbBwMADPIANCf/kZbH/rzuP8A0OGtKsi9stVl123urW5tI4I7eWMiW3ZyCzRns65+6fpjvmpfI1v/AKCGn/8AgC//AMdoA0qKzfI1v/oIaf8A+AL/APx2o4YtfZCZr3TkbewAFm5+UMdp/wBb3GD+NAGtVHV/+QXN/wAB/wDQhUfka3/0ENP/APAF/wD47VbULPXZ7CeNL7T3coSi/Y3Xcw5Az5pxyKirFyhKK6ozrRc6corqmYlFaH/CNah/0FLX/wAA2/8AjlH/AAjWof8AQUtf/ANv/jleN9QrHhf2bX8vvM+irh8OaqLhFGoWhiKMWf7I2Q2RgY8zuC35e9Sf8I1qH/QUtf8AwDb/AOOUfUKwf2bX8vvM+itD/hGtQ/6Clr/4Bt/8cqOHw5qrITNqFojb2AAtGPyhjtP+s7jB/Gj6hWD+za/l95TorQ/4RrUP+gpa/wDgG3/xyo5vDmqqgMOoWjtvUEG0YfKWG4/6zsMn8KPqFYP7Nr+X3lOitD/hGtQ/6Clr/wCAbf8Axyj/AIRrUP8AoKWv/gG3/wAco+oVg/s2v5feZ9FXB4c1U3DqdQtBEEUq/wBkbJbJyMeZ2AX8/apP+Ea1D/oKWv8A4Bt/8co+oVg/s2v5feZ9FaH/AAjWof8AQUtf/ANv/jlRweHNVa3iafULSOUoC6LaMQrY5APmc80fUKwf2bX8vvKdRy/620/6/Lf/ANHJWp/wjWof9BS1/wDANv8A45UcnhrUg9u4v7aUR3MMjILZkJVZFY4PmHHAPatKWCqxnGT6M0o5fWhUjJ9GjoNSsU1TSrzT5ZJYo7qB4Gkhba6hlKkqexGeDXxd4m0K48L+JNQ0W7O6W0lKb8AeYpGUfAJxuUqcZ4zjrX23XKa34B0nXvGmj+Jbvf8AaNMUhY1YgSsrBoixzwEYucAclhk4GD9NgsW8PJ32f9I9uUbkPw18IzeD/B9nY3NxM1zIvnXMLFSkcrckLjPQYXg4O3OASa2Lrw8jzx3VpdXEd1HL5qG4nlniyQQf3bPjoxxjGP0rZorjlJzk5PdlHOzeGHksrS0W9VYre1NkCYcs0LCMNzu4fMeQ3QZ+6cZpJfDl7cSS3E2pQPdtOZI5fshwqbJEEZXzOQFlbGMc5JzmujoqQOYHhExlRFfgJCQ1uHh3FW8yKQ7zuG4boV6beCeelbmm2X9n6fFbGTzGTJZ9u0FiSTgdhknA7VbooAKiuomntJoUkMTyIyrIvVSRjI+lS0UAcjD4avY7A2z2emmEXKTiz81vJceT5ZQ/u+BuAkzg5b6bj0un28lnptrbSymaSGFI3lPVyAAT+PWrNFAGF4m0a41m2jjg8glVkXExICMy4WVcA/Mp5HTqeRSRaNKPE6ax9mtreR4dlxIlw8jSfKBt2lQAAR94cnHTnjeooAKx7mwu/wDhKLXUre1szEts8E0jylZW3PGegQ5ChGxk9W7da2KKAMLw7o1xpHned5A3RxxnyST5rru3TPkD533DPX7o5NbjDKkYByOh70tFAHInw3c3HhqTSrvTNOZVuJJoI47yRETdJIw6RjG0MoAxg89Mc9TbpJFaxRyyCSRUCu4XG4gcnHapaKAMbxFpk+qwW8MNtZyqJQ0j3DlXjX/pmQjYY8c8EDpzgiGDRrz/AISL+05ltk3N5jOjlpAPKVDDyoym4F85HP8ACOtb9FABXONotzJq2pTy6fpxt7qFoMRztG0iFeTJiPli2BnJ2jOOc56OigDL8P6Y+kaUtm6RRqsjtHFE5dY1LEhdxALdepHerl7CbixmhEUUpdCuyU4Vs+uVb+R+lWKKAOOPhbUH0i2s3FlmATon7w4i8xgyzLhABInzAABRzwVHyjsaKKAMPXtKutQubGe0gtGktZVlDzPtPDA7QfLZgDjqGHuGGVK6ZorWGv6hfRwQW8V1y4SZpGmfcTvO4DZwTwCRz7Vt0UAFcknhrVI/7USWezulvZopyzK0ZkKMpKOPmBVlBX2AAwQcDraKAKOj2cmn6XDbSlNybjtjOVQFiQi8D5VBCjgcAcCr1FFABRRRQAUUUUAFFFFAH//Z",
      "text/plain": [
       "<Figure size 320x240 with 2 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "step = 0\n",
    "while t <= T:\n",
    "    step += 1\n",
    "    phiVec[:] -= pVec[:]*dt/2\n",
    "    pVec[:]   += S*phiVec[:]\n",
    "    t += dt\n",
    "    # set the values on the Dirichlet boundary\n",
    "    op.model.g = numpy.sin(2*numpy.pi*5*t)\n",
    "    op.setConstraints(p)\n",
    "    phiVec[:] -= pVec[:]*dt/2\n",
    "phi.plot(gridLines=None, clim=[-0.02,0.02])"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
