{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b25661c1",
   "metadata": {},
   "source": [
    "# Mean Curvature Flow (revisited)\n",
    "\n",
    "[As discussed before](mcf_nb.ipynb)\n",
    "we can simulate the shrinking of a sphere under mean curvature flow\n",
    "using a finite element approach based on\n",
    "the following time discrete approximation:\n",
    "\\begin{align}\n",
    "\\int_{\\Gamma^n} \\big( U^{n+1} - {\\rm id}\\big) \\cdot \\varphi +\n",
    "\\tau \\int_{\\Gamma^n} \\big(\n",
    "\\theta\\nabla_{\\Gamma^n} U^{n+1} + (1-\\theta) I \\big)\n",
    "\\colon\\nabla_{\\Gamma^n}\\varphi\n",
    "=0~.\n",
    "\\end{align}\n",
    "Here $U^n$ parametrizes $\\Gamma(t^{n+1})$ over\n",
    "$\\Gamma^n:=\\Gamma(t^{n})$,\n",
    "$I$ is the identity matrix, $\\tau$ is the time step and\n",
    "$\\theta\\in[0,1]$ is a discretization parameter.\n",
    "\n",
    "If the initial surface $\\Gamma^0$ is a sphere of radius $R_0$,\n",
    "the surface remains sphere and we have an exact formula for the evolution\n",
    "of the radius of the surface\n",
    "\n",
    "$$R(t) = \\sqrt{R_0^2 - 4t}.$$\n",
    "\n",
    "To compare the accuracy of the surface approximation we compute an\n",
    "average radius of the discrete surface in each time step $t^n$ using\n",
    "\n",
    "$$R_h^n = \\frac{ \\int_{\\Gamma^n} |x| }{ |\\Gamma^n| }.$$\n",
    "\n",
    "Computing $R_h^n$ requires a grid traversal and a number of calls to\n",
    "interface methods on each element. Doing this on the Python side has a\n",
    "potential performance impact which we investigate here by comparing a\n",
    "pure python implementation with a hybrid approach where computing $R_h^n$\n",
    "is implemented in C++ using the `dune.generator.algorithm` functionality."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "210af8c0",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:20:32.256398Z",
     "iopub.status.busy": "2023-07-09T15:20:32.255990Z",
     "iopub.status.idle": "2023-07-09T15:20:33.162042Z",
     "shell.execute_reply": "2023-07-09T15:20:33.161093Z"
    }
   },
   "outputs": [],
   "source": [
    "import time, io\n",
    "import pickle\n",
    "import numpy\n",
    "from matplotlib import pyplot as plt\n",
    "from matplotlib.ticker import ScalarFormatter\n",
    "\n",
    "from ufl import *\n",
    "import dune.ufl\n",
    "from dune.generator import algorithm\n",
    "import dune.geometry as geometry\n",
    "import dune.fem as fem\n",
    "\n",
    "def plot(ct, ct2):\n",
    "    fig, ax = plt.subplots()\n",
    "    plt.loglog(ct[0][:], ct[1][:],'*-', markersize=15, label='hybrid')\n",
    "    plt.loglog(ct2[0][:], ct2[1][:],'*-', markersize=15, label='python')\n",
    "    plt.grid(True)\n",
    "    for axis in [ax.xaxis, ax.yaxis]:\n",
    "        axis.set_major_formatter(ScalarFormatter())\n",
    "        axis.set_minor_formatter(ScalarFormatter())\n",
    "    yticks = ax.yaxis.get_minor_ticks()\n",
    "    for t in yticks:\n",
    "        t.label1.set_visible(False)\n",
    "    xticks = ax.xaxis.get_minor_ticks()\n",
    "    for t in xticks:\n",
    "        t.label1.set_visible(False)\n",
    "    plt.yticks(numpy.append(ct[1], ct2[1]))\n",
    "    plt.xticks(ct[0])\n",
    "    plt.legend(loc=\"upper left\")\n",
    "    plt.xlabel('Number of Grid Elements (log)',fontsize=20)\n",
    "    plt.gcf().subplots_adjust(bottom=0.17, left=0.16)\n",
    "    plt.ylabel('Runtime in s (log)',fontsize=20)\n",
    "\n",
    "# polynomial order of surface approximation\n",
    "order = 2\n",
    "\n",
    "# initial radius\n",
    "R0 = 2.\n",
    "\n",
    "# end time\n",
    "endTime = 0.1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02609243",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "Main function for calculating the mean curvature flow of a given surface.\n",
    "If first argument is `True` the radius of the computed surface is\n",
    "computed using an algorithm implemented in C++ otherwise the computation\n",
    "is done in Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "968746ec",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:20:33.166718Z",
     "iopub.status.busy": "2023-07-09T15:20:33.166374Z",
     "iopub.status.idle": "2023-07-09T15:20:33.172629Z",
     "shell.execute_reply": "2023-07-09T15:20:33.171783Z"
    }
   },
   "outputs": [],
   "source": [
    "def calcRadius(surface):\n",
    "    R,vol = 0, 0\n",
    "    for e in surface.elements:\n",
    "        rule = geometry.quadratureRule(e.type, 4)\n",
    "        for p in rule:\n",
    "            geo = e.geometry\n",
    "            weight = geo.volume * p.weight\n",
    "            R   += geo.toGlobal(p.position).two_norm * weight\n",
    "            vol += weight\n",
    "    return R/vol\n",
    "\n",
    "code = \"\"\"\n",
    "#include <dune/geometry/quadraturerules.hh>\n",
    "template< class Surface >\n",
    "double calcRadius( const Surface &surface ) {\n",
    "  double R = 0, vol = 0.;\n",
    "  for( const auto &entity : elements( surface ) ) {\n",
    "    const auto& rule = Dune::QuadratureRules<double, 2>::rule(entity.type(), 4);\n",
    "    for ( const auto &p : rule ) {\n",
    "      const auto geo = entity.geometry();\n",
    "      const double weight = geo.volume() * p.weight();\n",
    "      R   += geo.global(p.position()).two_norm() * weight;\n",
    "      vol += weight;\n",
    "    }\n",
    "  }\n",
    "  return R/vol;\n",
    "}\n",
    "\"\"\"\n",
    "switchCalcRadius = lambda use_cpp,surface: \\\n",
    "             algorithm.load('calcRadius', io.StringIO(code), surface) \\\n",
    "             if use_cpp else calcRadius"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36ebc787",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "Timings for a number of different grid refinements is dumped to disk"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "c88797d4",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:20:33.176180Z",
     "iopub.status.busy": "2023-07-09T15:20:33.175821Z",
     "iopub.status.idle": "2023-07-09T15:20:33.190717Z",
     "shell.execute_reply": "2023-07-09T15:20:33.189792Z"
    }
   },
   "outputs": [],
   "source": [
    "from dune.fem.view import geometryGridView as geoGridView\n",
    "from dune.fem.space import lagrange as solutionSpace\n",
    "from dune.fem.scheme import galerkin as solutionScheme\n",
    "def calculate(use_cpp, gridView):\n",
    "    # space on Gamma_0 to describe position of Gamma(t)\n",
    "    space = solutionSpace(gridView, dimRange=gridView.dimWorld, order=order)\n",
    "    u = TrialFunction(space)\n",
    "    v = TestFunction(space)\n",
    "    x = SpatialCoordinate(space.cell())\n",
    "    positions = space.interpolate(x, name=\"position\")\n",
    "\n",
    "    # space for discrete solution on Gamma(t)\n",
    "    surface = geoGridView(positions)\n",
    "    space = solutionSpace(surface, dimRange=surface.dimWorld, order=order)\n",
    "    solution  = space.interpolate(x, name=\"solution\")\n",
    "\n",
    "    # set up model using theta scheme\n",
    "    theta = 0.5   # Crank-Nicolson\n",
    "\n",
    "    I = Identity(3)\n",
    "    dt = dune.ufl.Constant(0,\"dt\")\n",
    "\n",
    "    a = (inner(u - x, v) + dt * inner(theta*grad(u)\n",
    "        + (1 - theta)*I, grad(v))) * dx\n",
    "\n",
    "    scheme = solutionScheme(a == 0, space, solver=\"cg\")\n",
    "\n",
    "    Rexact = lambda t: numpy.sqrt(R0*R0 - 4.*t)\n",
    "    radius = switchCalcRadius(use_cpp,surface)\n",
    "\n",
    "    scheme.model.dt = 0.02\n",
    "\n",
    "    numberOfLoops = 3\n",
    "    times = numpy.zeros(numberOfLoops)\n",
    "    errors = numpy.zeros(numberOfLoops)\n",
    "    totalIterations = numpy.zeros(numberOfLoops, numpy.dtype(numpy.uint32))\n",
    "    gridSizes = numpy.zeros(numberOfLoops, numpy.dtype(numpy.uint32))\n",
    "    for i in range(numberOfLoops):\n",
    "        positions.interpolate(x * (R0/sqrt(dot(x,x))))\n",
    "        solution.interpolate(x)\n",
    "        t = 0.\n",
    "        error = abs(radius(surface)-Rexact(t))\n",
    "        iterations = 0\n",
    "        start = time.time()\n",
    "        while t < endTime:\n",
    "            info = scheme.solve(target=solution)\n",
    "            # move the surface\n",
    "            positions.assign(solution)\n",
    "            # store some information about the solution process\n",
    "            iterations += int( info[\"linear_iterations\"] )\n",
    "            t          += scheme.model.dt\n",
    "            error       = max(error, abs(radius(surface)-Rexact(t)))\n",
    "        print(\"time used:\", time.time() - start)\n",
    "        times[i] = time.time() - start\n",
    "        errors[i] = error\n",
    "        totalIterations[i] = iterations\n",
    "        gridSizes[i] = gridView.size(2)\n",
    "        if i < numberOfLoops - 1:\n",
    "            gridView.hierarchicalGrid.globalRefine(1)\n",
    "            scheme.model.dt /= 2\n",
    "    eocs = numpy.log(errors[0:][:numberOfLoops-1] / errors[1:]) / numpy.log(numpy.sqrt(2))\n",
    "    try:\n",
    "        import pandas as pd\n",
    "        keys = {'size': gridSizes, 'error': errors, \"eoc\": numpy.insert(eocs, 0, None), 'iterations': totalIterations}\n",
    "        table = pd.DataFrame(keys, index=range(numberOfLoops),columns=['size', 'error', 'eoc', 'iterations'])\n",
    "        print(table)\n",
    "    except ImportError:\n",
    "        print(\"pandas module not found so not showing table - ignored\")\n",
    "        pass\n",
    "    return gridSizes, times"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b438021",
   "metadata": {},
   "source": [
    "Compute the mean curvature flow evolution of a spherical surface. Compare\n",
    "computational time of a pure Python implementation and using a C++\n",
    "algorithm to compute the radius of the surface for verifying the\n",
    "algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "e40411b2",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:20:33.194649Z",
     "iopub.status.busy": "2023-07-09T15:20:33.194376Z",
     "iopub.status.idle": "2023-07-09T15:21:12.453125Z",
     "shell.execute_reply": "2023-07-09T15:21:12.452100Z"
    }
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "GridParameterBlock: Parameter 'bisectioncompatibility' not specified, defaulting to '0' (false).\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "time used: 0.4843320846557617\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "time used: 2.6900479793548584\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "time used: 12.148129940032959\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "   size     error       eoc  iterations\n",
      "0   318  0.001060       NaN          76\n",
      "1   766  0.000605  1.619153         208\n",
      "2  1745  0.000275  2.275142         420\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "GridParameterBlock: Parameter 'bisectioncompatibility' not specified, defaulting to '0' (false).\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "time used: 0.7143912315368652\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "time used: 3.8385894298553467\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "time used: 15.088636875152588\n",
      "   size     error       eoc  iterations\n",
      "0   318  0.001060       NaN          76\n",
      "1   766  0.000605  1.619153         208\n",
      "2  1745  0.000275  2.275142         420\n"
     ]
    }
   ],
   "source": [
    "# set up reference domain Gamma_0\n",
    "results = []\n",
    "from dune.alugrid import aluConformGrid as leafGridView\n",
    "gridView = leafGridView(\"sphere.dgf\", dimgrid=2, dimworld=3)\n",
    "results += [calculate(True, gridView)]\n",
    "\n",
    "gridView = leafGridView(\"sphere.dgf\", dimgrid=2, dimworld=3)\n",
    "results += [calculate(False, gridView)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fa829fa2",
   "metadata": {},
   "source": [
    "Compare the hybrid and pure Python versions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "a2654b75",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2023-07-09T15:21:12.459872Z",
     "iopub.status.busy": "2023-07-09T15:21:12.459655Z",
     "iopub.status.idle": "2023-07-09T15:21:13.057899Z",
     "shell.execute_reply": "2023-07-09T15:21:13.057153Z"
    }
   },
   "outputs": [
    {
     "data": {
      "image/jpeg": "/9j/4AAQSkZJRgABAQEAMgAyAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADQASADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAopG+6eSOOorjtO1HWH1CzeS61GW3Es0c1vNpbRkoHl2Sl9gGdoj+UYzx64IB2VQXt0ljYXF5IrMkETSsqYyQoJIGe/Fc1dXuuPf3qwSXUUQVmTNoSqqDFt2Hy2JZlMmeG2n+HjBnvLm/b4f3ct7bSPdPZzB1XarbcNhmB24O3BIwD147UAaf8Aad3/ANAPUP8AvuD/AOO0i6rdMWA0PUMqcH54PTP/AD196dd3kdtDLPNazSNu2oiwtKc7cgEIGIHvivPZ/iDdx+IIUOhSRBUMb2Tffd2xtP3c54GBjufWtqVCdT4UY1a8KVuZnoP9p3f/AEA9Q/77g/8AjtH9p3f/AEA9Q/77g/8AjtM0m+j1O1Dvp0ttMFBkilgdNpOeAWVd3TnFYdz4plsbySKbR4pojNNHE8L/ADHYURQVx1Z5I1BB/iycAVk007M1TUldG/8A2nd/9APUP++4P/jtH9p3f/QD1D/vuD/47XP2/imWW7Nn/ZllLcG7lgHlz4CBZmQeYNpKkgZHXdhjx0p+n+Jnvr7TLU2NpH9ocLLlvnI8hpMon93IA3EnkMMd6Qzd/tO7/wCgHqH/AH3B/wDHaP7Tu/8AoB6h/wB9wf8Ax2rR/dTL5cJ5U5CYHp70/wA5/wDn2l/Nf8aAKX9p3f8A0A9Q/wC+4P8A47R/ad3/ANAPUP8AvuD/AOO1d85/+faX81/xo85/+faX81/xoApf2nd/9APUP++4P/jtH9p3f/QD1D/vuD/47V3zn/59pfzX/Gjzn/59pfzX/GgCl/ad3/0A9Q/77g/+O0f2nd/9APUP++4P/jtXfOf/AJ9pfzX/ABo85/8An2l/Nf8AGgCl/ad3/wBAPUP++4P/AI7SJqt04yuh6hjJH34Oxx/z1q95z/8APtL+a/41Dbyv5Z/0eU/O/df7x96AIP7Tu/8AoB6h/wB9wf8Ax2j+07v/AKAeof8AfcH/AMdq75z/APPtL+a/40ec/wDz7S/mv+NAFL+07v8A6Aeof99wf/HaP7Tu/wDoB6h/33B/8dq75z/8+0v5r/jR5z/8+0v5r/jQBS/tO7/6Aeof99wf/HaP7Tu/+gHqH/fcH/x2rvnP/wA+0v5r/jR5z/8APtL+a/40AUv7Tu/+gHqH/fcH/wAdo/tO7/6Aeof99wf/AB2rvnP/AM+0v5r/AI0ec/8Az7S/mv8AjQBS/tO7/wCgHqH/AH3B/wDHaP7Tu/8AoB6h/wB9wf8Ax2rvnP8A8+0v5r/jR5z/APPtL+a/40AUv7Tu/wDoB6h/33B/8do/tO7/AOgHqH/fcH/x2rvnP/z7S/mv+NZOt6zc2EQNrCjTDLNHJydvr8rcfjWdWrGlBzlsjSlTlUmoR3ZYTVbpxldD1DGSPvwdjj/nrT7fVGmv0s5tPurZ3ieVWlMZBClQR8jtz84rnfDGr6peSPCIo5IEcySufvDcScDkd81vO7P4lst0bJizuPvEc/PD6E1nhsRHEU1UitC8Th5Yep7OT1NSikZlRSzEKoGSScACqtrqunX0jR2l/a3Ei/eWKZXI+oBroMC0c4OOT2zWFZ69dXNzZwvYwL58s8UgjuS7RmJ3UtjYMplFGSRy4GPXeIyMHpWbB4e0S2l82DR9PikwRvjtkU4OcjIH+035n1oAiutVv47vUbW10xLqa2hgmhRbkIZhIzqc5XCkbGPU59qrapqaXXgW7vWjdfOspMoiNIUYoQRwOgORnGOK0n0bSpJZZH0yzaSZFjkYwKS6rjapOOQMDA7YFQa5FHB4U1KGGNY4kspVREGAoCHAAHQUAWZL63tY5Zp5BGm/A8z5MnaOPmxzxXB3V14eudVe5e0uG81zK9wT++WQY27TngDkY/nivQ/mR3/dswY5BBHoB3PtWJLoFvdX0143n/axMHWQbcJgAgYzyMYrkxP1tWeGlbv/AFf8Dqw6wjusRG/b+v1NSw1O11GASW8gbgFkyCyZ6ZAJxVC78Q6CLjybuVfMgeR1823cgNEu5ipK4JUenrWv5jf88X/Nf8aw5fC2nT3RuZYLl5Dc/aDukBGdyttAJwBuRCcYJ2gEkZB6YqSVpO7OeTTd4qyHxeLdMkuI4WW7jMkssSs9s4XckoiOTjgFmGD09SDxTz4s0VUDm6k2lS4P2eXlMbt/3fuYBO7ocHmpP7E0/ez/AGKUlnaTmYkBmkWQ4G7jLqGwOOvqajXw5pSxPGNPkKvE0JBmY4jK7Sgy3C4JAA4GeMVRJqSSKk6Eh/ut0QnuPSl+0p/dl/79N/hSjc0oYoVAUjkjvj0+lSUARfaU/uy/9+m/wo+0p/dl/wC/Tf4VLRQBF9pT+7L/AN+m/wAKPtKf3Zf+/Tf4VLRQBF9pT+7L/wB+m/wo+0p/dl/79N/hUtFAEX2lP7sv/fpv8Kht7hBGfll++/8Ayyb+8fardRW3+qb/AK6P/wChGgA+0p/dl/79N/hR9pT+7L/36b/CpaKAIvtKf3Zf+/Tf4UfaU/uy/wDfpv8ACpaKAIvtKf3Zf+/Tf4UfaU/uy/8Afpv8KlooAi+0p/dl/wC/Tf4UfaU/uy/9+m/wqWgnAyelAEX2lP7sv/fpv8Kjm1C2t4/MmZ41zjLRsOfTpUJv3uWKaegl7Gd/9Wv0/vH6fmKkgsEjlE87tcXH/PR/4f8AdHRf89ay9o5aU/v6f8H+tTX2ajrP7uv/AAP60KzXVzeMVRZrSD++YmMjfQYwv1PPsKzPEF/BoOivNFpLXluWH2gOzI3sxJU7ua1bvVlS4NnZRG7vR1jU4WP3dui/TknsDVWbw6mqQt/bM7XMrjAVCUjiHcIP03HJ5PTpWtOjBNSq6+v9af1uY1K02nGlp/X4/wBbHK+CvFFxqN60Q0fe+9jLdxkgRxsxYKQFOcdB7D2rsnkWTxLZbQwxZ3H3kK/xw+tR6PoOnaXPJc2MHkM4MTojHawVjgkeo9amn/5GWx/687j/ANDhrSq6blemrIzoqoo2qSuyt4m03S7vTZLzVrL7dBYxST/ZnkxHJgZ+ZWIRj8vBfgZPTJrhrrUNEltbKa4+F+o28dw6rbzxQW0MiM33drLIrIx6DkEkgDk16Rq8VlcaLfw6kVFhJbyJcljgCMqQ2T24zXnOmXMd7qGlW+q6v4kl0oXET6eb7TkgiuJFOYt8iqGPIBG4LuIHU8Vmanoei6ja6to1re2bSmCRcDzgRIpU7WVs87gQQfcGr9U9L02DSbEWlsXMfmSSkucktI7OxP8AwJjVygArM8R7v+EY1TZjP2SXr6bTn9K06yvE3l/8Ivqnmfd+yyY+u04/XFAGrUUP+tuP+ug/9BWpaih/1tx/10H/AKCtAEtFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFRW3+qb/ro/wD6EalqK2/1Tf8AXR//AEI0AS0UUUAFFFFABRUVxcw2se+aQICcDuSfQDqT7Cqn+mX396ztz/39b+i/qfpWcqiTstWXGm2rvRE1xfxwyeTGrTXBGRFHyR7k9FHuaiFlLdndqDhk7W8Z+Qf7x6t+PHtVq3tobWPy4Ywi5yfUn1J6k+5qjc6sWuHs9NiF3dqcPziOE/7bdj/sjJ9u9CpSqfH93T59/wAvIcqsafwff1+Xb+tS5c3Vtp9sZriVIYU4yeB7Ae/sKzs6hrHTzdPsT36Tyj/2mP8Ax7/dqa10kLcLeX0pu7wfddhhIvZF/h+vJPc1pVvdR2MLOW5BaWdvY24gtYVijHOF7nuT6n3NT0UVDd9y0raIitv9U3/XR/8A0I1Sn/5GWx/687j/ANDhq7bf6pv+uj/+hGqU/wDyMtj/ANedx/6HDQBT8V6polppcmna3NNHb6jDLARFBJIWUrtblFOOG71xdhqlvqeoaZp+oeL5b60huoXhgTRJoJJpEYGPzJCCuAwUnAXOOcCu/wBXt9YlEUuj31vBLHndDcweZFMDjqQQykY4IOOTkHjGUq+Obo+VM+g6fGeDPAZbl8eysEAP1JHsaAOoopsaskaqzl2AALsBlj6nHFOoAKzPEbbPDGqHaT/okowB6qRWnWZ4jbZ4Y1Q7Sf8ARJRgD1UigDTqKH/W3H/XQf8AoK1LUUP+tuP+ug/9BWgCWiiigAooooAKKKKACiiigAooooAKKKKACorb/VN/10f/ANCNS1Fbf6pv+uj/APoRoAlooqpPfpHKYIEa4uB1jT+H/ePRf5+xqZSUVdlRi5OyLTMFUsxAA5JPaqJvZbs7dPQFO9zIPkH+6Orfy96FsHuWD6hIJcciBeI1/D+I/X8hV8DAwKj35+S/H/gfn6F+5Dzf4f8AB/rcq29hHDJ5zs01wRgyycn6Dso9hTry+trCDzrmURpnA7lj2AA5J9hVKbVnnme10qJbmZTtkmY4hiP+03c/7I59cVJZ6SkE/wBruZWu73GPPkH3R6IvRR9OT3JraNOMFroYSqSm9NSDy9Q1jmfzLCxP/LJWxPKP9oj7g9hz7jpWnbW0FnAkFtEkUSDCogwBUtFNyvp0BRS16hRRRUlBRRRQBFbf6pv+uj/+hGqU/wDyMtj/ANedx/6HDV22/wBU3/XR/wD0I1Sn/wCRlsf+vO4/9DhoAk1vUDpGg6jqSxGZrS1luBGDjeUUtj8cYrmJfGl42rxizgsp9KintLS6mWUlzNcEACPAwQu+MnPUN2xXaEBlIYAgjBBrn7W58HQGHTbSfQoytwHitYXhXE2eCqD+PPoM0AdDRRWHr2lXWoXNjPaQWjSWsqyh5n2nhgdoPlswBx1DD3DDKkA3KzfEP/Itar/15zf+gGsBvCeovqOqTLc2sMd5vBO0uXUuG2sAFONoKnLv1+XYOKu6lp00Pw9lsZ7jbNb6dtd4AMMUj7bgeCR9fpQB0lRQ/wCtuP8AroP/AEFaPJf/AJ+ZfyX/AAqGKJ/Mn/0iX747L/dX2oAt0VF5L/8APzL+S/4UeS//AD8y/kv+FAEtFReS/wDz8y/kv+FHkv8A8/Mv5L/hQBLRUXkv/wA/Mv5L/hR5L/8APzL+S/4UAS0VF5L/APPzL+S/4UeS/wDz8y/kv+FAEtFReS//AD8y/kv+FHkv/wA/Mv5L/hQBLRUXkv8A8/Mv5L/hSeS//PzL+S/4UATVnQanaC0MvmggzSIqqMsxDHgAcmsrW4W1ezeO0E1yYjkSjaFDdMKQPm9+w9eKpeHPDs1tqDS3iOroMxOhBUEHBz7159TFVfbKnSjeL662O+nhqXsXUqStJdOp0ey8vv8AWFrS3P8AAp/eMPcj7v0HPuKuQW8NtEI4Y1RB2UfrTfJf/n5l/Jf8KyXv7m9kaDR5WmKna904XyYz3AwPnPsOPUiu6nS1vu+7OKpV0tsuyNO91C20+IPcSYLHaiKCzufRVHJP0qh9mvtX5vS9nZHpaxv+8kH/AE0YdB/sr+JPSpbTRUtpmuXup57xxhp5ApbHovGFX2FXvJf/AJ+ZfyX/AArW6j8JlyuXxfcLDBFbQpDBGkcSDCogwAPYVJUXkv8A8/Mv5L/hR5L/APPzL+S/4VBZLRUXkv8A8/Mv5L/hR5L/APPzL+S/4UAS0VF5L/8APzL+S/4UeS//AD8y/kv+FAEtFReS/wDz8y/kv+FHkv8A8/Mv5L/hQAW3+qb/AK6P/wChGqU//Iy2P/Xncf8AocNT28T+Wf8ASJR879l/vH2qs6MniWy3SM+bO4+8Bx88PoBQA/X7SW/8OapZwBzLcWksSBHCMWZCBhjwDk9a5TQ7XU7Y6fDP8PNMtPLMavcQXMJEWCMuo25464zn3rsbzTLLUM/ardZd0Elud2f9XJjev0O1fyrFtfAPhS0u4rq20a3SeCRZEdS2UYEEHr9DQB0tFFYes6bd3mp2Nxb2dhIlsTIZJZTHLuH3VVgjYXPXB56dMggG5WZ4jLDwxqhVdx+ySjGccbTk1gy+Erl5tSdhBN9plMuXmOZT5u9AwaNlUIvygYYNwTjAq/qOnzWnw/msXvJPNt9OKPMgGXKx4P3geDj6+9AHRVFD/rbj/roP/QVo8l/+fmX8l/wqGKJ/Mn/0iX747L/dX2oAt0VF5L/8/Mv5L/hR5L/8/Mv5L/hQBLRUXkv/AM/Mv5L/AIUeS/8Az8y/kv8AhQBLRUXkv/z8y/kv+FHkv/z8y/kv+FAEtFReS/8Az8y/kv8AhR5L/wDPzL+S/wCFAEtFReS//PzL+S/4VQe4nnkaGwmeRlOHmYL5aH8vmPsPxIqZTUdyowcti7c3cNogMrHLHCIoyzn0A71W+zT3/wA17+7g7Wynr/vkdfoOPrToNNEMjTNczSTsMNKwUn6Djgewqx5L/wDPzL+S/wCFRyOfx7dv8/6t6l86h8G/f/L+r+hKqqihVAVQMAAYArHg8Qad9iMyTGUtPLHHFGNzyMHPCqOvrnpg5OKwfFcB8RaZLb6dFPevA2VuFKqqvnG1SB859ccDucjFZfgvwXc6frLz6pC4kjTdbyRsGQNnDA/7Q7fia7oUYKm5SevY4J1qjqKMFddzs/sN5q3zamfItT0so2+9/wBdGHX/AHRx6lq1o40ijWONFRFGFVRgAegFM8l/+fmX8l/wo8l/+fmX8l/wrBybOiMUiWiovJf/AJ+ZfyX/AAo8l/8An5l/Jf8ACpKJaKi8l/8An5l/Jf8ACjyX/wCfmX8l/wAKAJaKi8l/+fmX8l/wo8l/+fmX8l/woAloqLyX/wCfmX8l/wAKPJf/AJ+ZfyX/AAoAloqLyX/5+ZfyX/CjyX/5+ZfyX/CgAtv9U3/XR/8A0I1Sn/5GWx/687j/ANDhqe3ifyz/AKRKPnfsv94+1VnRk8S2W6RnzZ3H3gOPnh9AKAJtZgu7nQtQt9Pl8m9ltpEt5M42SFSFOfY4NeZaDp12t5LpNj4Z1HTUbWbS+8yeMLHCkcUIlO/JDszJIvGc78mvS9dkmi8PalJb3UdpMlrK0dxL92Fghw7ewPJ+lef+HpNBup9MuBa+OHuneJhLdfb2iL5GGc58spnk/wAOPagD1CiiigArM8RMF8M6qSQB9klHP+4a06zPEShvDOqggEfZJTz/ALhoA06ih/1tx/10H/oK1LUUP+tuP+ug/wDQVoAlooooAKKKKACiiigAqG4uobSPfM+0E4AxksfQDqTUEt8zytb2SCaZTh2J+SP/AHj3PsOfpTrexWKTz5nM9yRgysOg9FH8I/yc1k5uWkPv6f8ABNVBR1n93X/gEXk3Ooc3O63tj0gU/O4/2yOg9h+J7VejjSKNY40VEUYCqMACnVl3OqvJcPZ6ZEtzcqcSOTiKH/ebuf8AZHP061dOlZ33fcipV0s9F2Ld7f22nwiW4k2gnaigEs7eiqOSfYVn/ZLzWPm1ANbWR6Wat80g/wCmjDt/sjj1J6VZstKS3mN1cStdXzDBnkH3R/dQdFX2H4k1oVrdR+Ey5XL4tu3+Y2ONIo1jjRURRhVUYAHoBTLb/VN/10f/ANCNS1Fbf6pv+uj/APoRqCyWiiigAooooAKKKKACiiigAooooAKKKKAIrb/VN/10f/0I1Sn/AORlsf8ArzuP/Q4au23+qb/ro/8A6EapT/8AIy2P/Xncf+hw0AReIpLKTR7/AE+7uY4Rc2NwWMkZdRGFAdioxkDeMjIzmuI0PxG/23TrL/hYlnfDzY4vKOjlHmGQNu7dgE9M475rsfGDa7/wjF8nh22SfUZYZI4y1x5LR5RsOhwQWDbcA4HuKxv7U8Uajc2llPDo2mKLmF5pYtWM0pVXVmjCeUuSwG3r0Y0AdtRRRQAVm+If+Ra1X/rzm/8AQDWlWb4h/wCRa1X/AK85v/QDQBpVFD/rbj/roP8A0FalqKH/AFtx/wBdB/6CtAEtFFFABRRVOe+xKbe1j8+4H3hnCx/7x7fTrUymoq7KjFydkTz3EVrEZZnCIOMnufQep9qqbLrUf9ZvtbU/wA4kkHuf4R7Dn6VJBY7ZRcXMnn3PZiMKnso7fz96uVnyyn8Wi7f5/wCRfNGHw6vv/l/mMihjgiWKJFRFGAqjAFMuruCyt2nuZViiXqzH9Pc+1VLzVRDP9jtIjdXxGfKU4CD1dv4R+p7A0210om4W91GUXV4vKcYjh9kXt/vHk/pXQoJLXRGDm29NWQ7b7Wvv+bYaef4fuzzD3/55r/49/u1qW1tBZ26QW8SRRIMKiDAFS0UOV9OgKNteoUUUVJQVFbf6pv8Aro//AKEalqK2/wBU3/XR/wD0I0AS0UUUAFFFFABRRRQAUUUUAFFFFABRRRQBFbf6pv8Aro//AKEapT/8jLY/9edx/wChw1dtv9U3/XR//QjVKf8A5GWx/wCvO4/9DhoAXX7ee78OapbWvm/aJrSWOLyWCvvKEDaSQAc4xkge4ritB0uO2k01ZfhRb2M8bRhrtGsn8lgR+8DB95x1yBu49a7zUxC2k3i3EU00BgcSRQBjI67TkKF53EcDHOeleZppWk3Gr6S+g+HfEltdw30Mjy3guY4ViDAvv8x8H5c4A5Jx2zQB6tRRRQAVm+If+Ra1X/rzm/8AQDWlWZ4j3f8ACMapsxn7JL19Npz+lAGnUUP+tuP+ug/9BWpaih/1tx/10H/oK0AS1HNNFbxNLM6oi9WY4FQXF8I5fs8CGe5xny1OAvux7D9fQGmw2JaVbi8cTTjlRjCR/wC6PX3PNZOo2+WGr/Bf12/I0UEleen5v+u/5jM3Wo/d32tqe/SWQf8Aso/X6VcggitohFDGqIOwqSqN9qkNk6wKjz3cgzHbxcu3ueyr7nAqoUtb7v8Ar7iZ1dLbL+vvLcsscETyzSLHGgyzucAD1JrJ+0Xus8WZezsT1uWXEko/6Zqfuj/aP4DvT4tLmvJUudXdJXU7o7VP9TEex5++3ufwArWrbSO2rMrOW+iK9nY29hB5NtEEXOSc5LHuSTyT7mrFFFS23qykktEFFFFIYUUUUAFRW3+qb/ro/wD6EalqK2/1Tf8AXR//AEI0AS0UUUAFFFFABRRRQAUUUUAFFFFABRRRQBFbf6pv+uj/APoRqlP/AMjLY/8AXncf+hw1dtv9U3/XR/8A0I1Sn/5GWx/687j/ANDhoAxfH2oGx07TY31C60+0ur9Ybu6tAfMjj8qR/lIBIyyKuQO9cl4T1PRNRmtYtd8Q6jqd3b37x6dDcpKq4WYiGRwqgO5G07nzjI4HJr1migArJ1HWmsNQhtlthIjGLzXMm0qJJRGu0YO7k5PIwPXNa1VrrTrK+Km7s7e4KAhTNEr7QeoGR3oAydQ169sjrBXT7eVNOtftIb7Uw3/eO0/u/lO1Se/VfXNTeK5oYfCuptOHKm3dRsRmO4g4+6DxnHPT1rRn0+yuYZ4bi0t5Yrg5mSSMMspwB8wI54AHPoPSqOvrHbeE9SjiiCxpZyIqRrgKNhAwOwH8qALF7fvb2rS29rLcOvPlhWUkexxXK2fiTUNRvpbeCBws8m8lDlo0wAecHHTriu2kQSRPGSQGBGVOCM+lZlho1jZ3bS28RjeI7AQx+YFR19eea4cTRr1KkXCdo9f6sduHrUadOSnG8un9XLVv9mtYvLhikVc5P7pySfUnHJ96lN1GoJIkAHJJibj9KivtRt9PRTMWaSQ4ihjG55D6KO/8h3qkNPudVIk1bCW/VbFGyv8A20P8R9vu/XrXfGCS7I4JTbfdkb6vPqZ8vSQ6W5+9fNCzL9Ixj5j7n5fr0q5Y21np6MIUnaSQ5lmkjdnkPqxxz/IdqvgBVCqAAOAB2pabl0WwlHW73IvtKf3Zf+/Tf4UfaU/uy/8Afpv8KloqSyL7Sn92X/v03+FH2lP7sv8A36b/AAqWigCL7Sn92X/v03+FH2lP7sv/AH6b/CpaKAIvtKf3Zf8Av03+FH2lP7sv/fpv8KlooAi+0p/dl/79N/hUNvcIIz8sv33/AOWTf3j7VbqK2/1Tf9dH/wDQjQAfaU/uy/8Afpv8KPtKf3Zf+/Tf4VLRQBF9pT+7L/36b/Cj7Sn92X/v03+FS0UARfaU/uy/9+m/wo+0p/dl/wC/Tf4VLRQBF9pT+7L/AN+m/wAKPtKf3Zf+/Tf4VLRQBF9pT+7L/wB+m/wo+0p/dl/79N/hUtFAEX2lP7sv/fpv8KPtKf3Zf+/Tf4VLRQBUt7hBGfll++//ACyb+8faqzyLJ4lstoYYs7j7yFf44fWr9t/qm/66P/6EapT/APIy2P8A153H/ocNAGlRRRQAVnXuswWN7DavFM7SFAzoBtj3uEQtkg8sccA9DnFaNUb7R7LUpEkuY3LoMBkmeM9QRnaRnBAIz0PIwaAKN74rsLKS/V0mYWXleZINioTI5QAMzAcFTknAHTOQQHaveJeeCry8iSTy7iweRVK/MAyZGR2xnmpZfDejy3M1z9gjjuJtpkmhJiclWLA7lIIOWJJHJ75qHXrCzi8GX1p9njNtb2TiNJBuCBUODz3GOvWgDQ1HUrTSbNru9lMUCkBn2M2M+uAa4+P4g2l1qNxZWRVXluAsE8iMyldqjO0DcTnOBx7kVvXOqeHZ7SeKPUNGZmRlAeaMrnHfnpXJ+HdD0DRNXS7/ALf0u5CKY5Vlkj6kA7k549Ppmuil7Llbnv0Oat7bmSht1O7sdLisnadne4u5BiS4l5dvYdlX2HFXqyP7X8Of9BDSv+/0f+NH9r+HP+ghpX/f6P8AxrBtt3Z0JJKyNeisc6x4bUZOo6UB6meP/Gl/tfw5/wBBDSv+/wBH/jSGa9FZH9r+HP8AoIaV/wB/o/8AGj+1/Dn/AEENK/7/AEf+NAGvRWONY8NkkDUdKyOo8+Pj9aX+1/Dn/QQ0r/v9H/jQBr0Vkf2v4c/6CGlf9/o/8aQax4bOcajpRwcHE8f+NAGxRWR/a/hz/oIaV/3+j/xpDrHhsEA6jpWT0Hnx8/rQBsVFbf6pv+uj/wDoRrN/tfw5/wBBDSv+/wBH/jUMGseHBGd2paUMyOBmeP8AvH3oA3aKyP7X8Of9BDSv+/0f+NIdY8NggHUdKyeg8+Pn9aANiisj+1/Dn/QQ0r/v9H/jR/a/hz/oIaV/3+j/AMaANeisj+1/Dn/QQ0r/AL/R/wCNH9r+HP8AoIaV/wB/o/8AGgDXorI/tfw5/wBBDSv+/wBH/jR/a/hz/oIaV/3+j/xoA16KyP7X8Of9BDSv+/0f+NH9r+HP+ghpX/f6P/GgDXorI/tfw5/0ENK/7/R/40f2v4c/6CGlf9/o/wDGgDStv9U3/XR//QjVKf8A5GWx/wCvO4/9Dhqrb6v4d8s51HS/vv8A8t4/7x96bBeabdeJrQafc2kxWzuN4t3VsfPDjOPxoA3qKKKACiiigAooooAy9X1S40x7XyrVJknlWEs0jJtZmCrkhCAMnuR6DJIByo/EwifTGXTVD6oI5ZysrERszJGMkJj0wW2Z24GTxXQXGn2V5IklzZ288icI0sSsV+hI4rHv5vCugXej2d5b2FpLcTGLT1+yjAkyDhSFwhzj0ycUAdDRVWHUrSfU7rTo5d13axxyTR7SNqybthzjBzsboe3NWqACiis/UdXt9MvNNtpklZ9QuDbRFACFYRvJlsnphD0zzigDQooooAKKKKACisnTNfi1bUr61trO78mzkaF7xwgiaRSAyL824kZ67ccHmm6p4o0jR7yOzu7lzdunmLb28Ek8m3ONxSNWIGe5GKANiiqun6jaarZrdWU3mwsSM7SpBBwQQQCCPQipEmdruWE28qoiqwmJXY5OcgYOcjAzkAcjBPOADK8Sa9N4ftkuVs0uYTuLjzirgKjOxVdpBwqMeSOQB3rPtvE7Q3FvZHSfIdpiLsCbK27PKFHOPnLM4PHTPX0159QsX8RwaPNbGS7ezluUkZFKiMMiMuSc5JZeMYIFUdVv/DXh64062u7JVmbzJLOK102Sdl2lS7KIkYryy5PHWgDoqKxU8UWEtxpUUcV5nU5pIYTLbPCVZEZzuWQKwGFOOOeO3NbVABRRRQAUUU2R1ijaRzhVBYn0AoAdRVfT7621TTrbULOTzbW5iWaGTaV3IwyDg4I4PerFABRWDe+LLCytdUumiuJIdNnjt5njVSGkfbwuSM7d656enJBreoAKwdU168sH1JI7CCZrSGKaMNclTMJC6hR8hw25MAcg7hyOlb1Y050eDxHBbzadCL6+jMiXJgT94Yip2luu4ZDAH+6SOlAFAeJCt/f6XpGmw3d1bKJTAl2qF3aTEvUcBSxOT1ORgcE7+nXi6hptreoMLPEsgGemRnHQfypbmws7wMLq0gnDKFYSxhsgHIBz2zzU0caRRpHGipGgCqqjAUDoAPSgB1FFFABRRRQAUUUUAFcN440m313xJ4b0y63CK4W8UsvDIfKBVh6EEAg+oFdzRQB4q1zqWs2PxBt7u3dtXs9Msre6SND+8kjacl0HcMuHA/2sVt6prum674p1CbS7pLqBPC14DNHymS6cA+o7jtkZr0+igDyfT/DGkpf+B4hajy9Q0yT7epYkXm2KNl83+/hjkA/TpxVC8FhDpWi2eoFxpNn4svLcICxEcCrcYU452AcHttznivZ6KAPHbrcdA1weG2VfC51a22skbyQLDtX7QUVCGMW/G4KQPv44zUF1Z2g8F+Jv7N1vS7mzl+xqbfRoHhhgfzgN65kcBmGMhSPugkZ6+00UAUtN0mw0a0a2020itoixcrGuNzEDLH1JwMk8mvKtA+weZ4a+ybv+E0+3D+2Ovn7cN5/nf7H93PH3dtexUUAcP8OdG0vTk12ay061tpP7XuoN8MKqfLWT5UyB90dh2qqmoWnhvxh4rGt339ltqhhkstRlACMghCbVZgU3IwY7W/vA4OTXoVFAHk0z3HiPTfD0Wq3c9/bP4peKG4kjERuLdYJ9p+RVBUgEZAwQTU2uWbafeeM7TRYGt0i0jT1WKzXayxebP5gQL0OzfjHPpXqdFAHmPhj/AIRr/haUP/CLeT9h/sOXd9mz5O/zos47b8Y3d+meau+Orq3s/G/hiW58QDQ4/sl+PtZaIc5t/l/eqy8/TPFeg0UAcC93b3mt+CJLXW11mMX12PtoaI7z9ml4/dgLx04Fcn4atmm1PTJNQ1vSLLxIl8Gu4vscov5DvO+NmMvzRsMgHbsAIIAxXtVFAHjU+iWR8Pajqyo6al/wlkkSXaSFZI0e/wDLZVYHKqVZsgdznrWhrFlPo7eNtM8NwtbRiysLgQW6thd8kizMqqQcmNOdpBJHBzzXqtFAHjKW1hH4d8WTaXreiTwN4eu1lstItHhTdsOJHzI4Dj5h2J3c9K2/7A0/T9e0SzsrRAmq6LeR3yN832sqsJUyZ+83zNyefmIr0uigDxnT7Xwo/wANtBjhv9D0+eD7O2ow3MY8mecQspjugpUg5JYbu69DXofgW6hu/C0L2+nQ2ECSyxpHbkmFwHI3xkgHY3UcDrXR0UAeW3oMfwy8RQS/8fMWtyif1Ja9V1P4o6EexFVfE/8AZP2/xb/be/8A4SDI/sL73m7fJXyvs2P4vN37tvfrxXqsVnbwXNxcxRKk1wVMzjq5UYBPvjj8B6VPQB5Nrv8AZv8Aa2t/8Jfj+0f7Nh/svOd2fKPmeRj/AJaebnO3nG3tWnYgy6B8LoIf+PkCCbI6iJbJw5+nzKPqwrrNb8O/24WWTV9TtbaSLypra1kRUlXnOSULAkHGVI4q/BpdjbSW8kNsiPbQfZoSB/q4uPlHoPlX8h6UAW6KKKAP/9k=",
      "text/plain": [
       "<Figure size 320x240 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plot(results[0],results[1])"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
